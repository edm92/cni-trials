# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table assessment (
  id_assessment             bigint auto_increment not null,
  assessment_type           integer,
  create_date               datetime,
  patient_id_patient        bigint,
  height                    double,
  weight                    double,
  body_fat                  double,
  bmi                       double,
  waist_circumference       double,
  hip_circumference         double,
  systolic_bp               double,
  diastolic_bp              double,
  advised_gp                integer,
  rbra_sbp                  double,
  rbra_dbp                  double,
  lbra_sbp                  double,
  lbra_dbp                  double,
  rank_abi                  double,
  rankba_pwv                double,
  lank_abi                  double,
  lankba_pwv                double,
  omron                     double,
  step_test                 integer,
  advised_ep                integer,
  msg_ep                    varchar(255),
  qcstpeak                  double,
  qcst15s                   double,
  qcst30s                   double,
  qcst60s                   double,
  qcst150s                  double,
  date_epreview             datetime,
  epnotes                   varchar(255),
  clear_exe_req             integer,
  clear_exe_rec             integer,
  clinical_notes            varchar(255),
  patient_meds              integer,
  diet_history_collected    integer,
  four_date_food_history    integer,
  measuring_cups            integer,
  sf12                      integer,
  dass21                    integer,
  survey                    integer,
  heart_rate_removed        integer,
  pedometer                 integer,
  accelerometer             integer,
  pathology                 integer,
  urine_bottle              integer,
  constraint pk_assessment primary key (id_assessment))
;

create table council (
  id_council                bigint auto_increment not null,
  council_date              datetime,
  council_type              integer,
  patient_id_patient        bigint,
  clinical_notes            varchar(255),
  confirmed_appoint         integer,
  pathology_referral        integer,
  feedback_letter           integer,
  certificat_provided       integer,
  weight                    double,
  body_fat                  double,
  constraint pk_council primary key (id_council))
;

create table linked_account (
  id                        bigint auto_increment not null,
  user_id                   bigint,
  provider_user_id          varchar(255),
  provider_key              varchar(255),
  constraint pk_linked_account primary key (id))
;

create table medication (
  id_medication             bigint auto_increment not null,
  trade_name                varchar(255),
  generic_name              varchar(255),
  reason                    varchar(255),
  start_date                datetime,
  stop_date                 datetime,
  ongoing                   integer,
  dose                      varchar(255),
  frequency                 varchar(255),
  route                     varchar(255),
  constraint pk_medication primary key (id_medication))
;

create table patient (
  id_patient                bigint auto_increment not null,
  hts_number                varchar(255),
  initials                  varchar(255),
  dob                       datetime,
  gender                    integer,
  constraint pk_patient primary key (id_patient))
;

create table security_role (
  id                        bigint auto_increment not null,
  role_name                 varchar(255),
  constraint pk_security_role primary key (id))
;

create table token_action (
  id                        bigint auto_increment not null,
  token                     varchar(255),
  target_user_id            bigint,
  type                      varchar(2),
  created                   datetime,
  expires                   datetime,
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id))
;

create table users (
  id                        bigint auto_increment not null,
  email                     varchar(255),
  name                      varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  last_login                datetime,
  active                    tinyint(1) default 0,
  email_validated           tinyint(1) default 0,
  constraint pk_users primary key (id))
;

create table user_permission (
  id                        bigint auto_increment not null,
  value                     varchar(255),
  constraint pk_user_permission primary key (id))
;


create table assessment_medication (
  assessment_id_assessment       bigint not null,
  medication_id_medication       bigint not null,
  constraint pk_assessment_medication primary key (assessment_id_assessment, medication_id_medication))
;

create table medication_assessment (
  medication_id_medication       bigint not null,
  assessment_id_assessment       bigint not null,
  constraint pk_medication_assessment primary key (medication_id_medication, assessment_id_assessment))
;

create table users_security_role (
  users_id                       bigint not null,
  security_role_id               bigint not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;

create table users_user_permission (
  users_id                       bigint not null,
  user_permission_id             bigint not null,
  constraint pk_users_user_permission primary key (users_id, user_permission_id))
;
alter table assessment add constraint fk_assessment_patient_1 foreign key (patient_id_patient) references patient (id_patient) on delete restrict on update restrict;
create index ix_assessment_patient_1 on assessment (patient_id_patient);
alter table council add constraint fk_council_patient_2 foreign key (patient_id_patient) references patient (id_patient) on delete restrict on update restrict;
create index ix_council_patient_2 on council (patient_id_patient);
alter table linked_account add constraint fk_linked_account_user_3 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_3 on linked_account (user_id);
alter table token_action add constraint fk_token_action_targetUser_4 foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_targetUser_4 on token_action (target_user_id);



alter table assessment_medication add constraint fk_assessment_medication_assessment_01 foreign key (assessment_id_assessment) references assessment (id_assessment) on delete restrict on update restrict;

alter table assessment_medication add constraint fk_assessment_medication_medication_02 foreign key (medication_id_medication) references medication (id_medication) on delete restrict on update restrict;

alter table medication_assessment add constraint fk_medication_assessment_medication_01 foreign key (medication_id_medication) references medication (id_medication) on delete restrict on update restrict;

alter table medication_assessment add constraint fk_medication_assessment_assessment_02 foreign key (assessment_id_assessment) references assessment (id_assessment) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_permission_02 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table assessment;

drop table assessment_medication;

drop table council;

drop table linked_account;

drop table medication;

drop table medication_assessment;

drop table patient;

drop table security_role;

drop table token_action;

drop table users;

drop table users_security_role;

drop table users_user_permission;

drop table user_permission;

SET FOREIGN_KEY_CHECKS=1;

