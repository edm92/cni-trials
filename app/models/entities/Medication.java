package models.entities;

import java.util.List;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.twirl.api.Html;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Medication extends Model{
	@Id
	public Long idMedication;
	
	@ManyToMany
	public List<Assessment> assess; 
	
	public String TradeName;
	public String GenericName;
	public String reason;
	
	public Date startDate;
	public Date stopDate;
	public int ongoing;
	public String dose;
	public String Frequency; 
	public String route;
	

}
