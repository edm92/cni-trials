package models.entities;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.twirl.api.Html;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Assessment extends Model{
	@Id
	public Long idAssessment;
	
	public Integer assessmentType;		// From Models.Type.getType
	
	public Date createDate; 	// All edits are stored in the DB; 
	
	@ManyToOne
	public Patient patient;
	
	// Anthro
	public Double height;
	public Double weight;
	public Double bodyFat;
	public Double BMI;
	public Double waistCircumference;
	public Double hipCircumference;
	// Physio
	public Double systolicBP;
	public Double diastolicBP;
	public int advisedGP ;	// 0 for no, 1 for yes
	public Double RBRaSBP;
	public Double RBRaDBP;
	public Double LBraSBP;
	public Double LBraDBP;
	public Double RAnkABI;
	public Double RAnkbaPWV;
	public Double LAnkABI;
	public Double LAnkbaPWV;
	public Double Omron;
	public int StepTest; // 0 for no, 1 for yes
	public int advisedEP; // 0 for no, 1 for yes
	public String msgEP; 
	public Double QCSTPeak;
	public Double QCST15S;
	public Double QCST30S;
	public Double QCST60S;
	public Double QCST150S;
	public Date dateEPReview;
	public String EPNotes;
	public int clearExeReq; // 0 for no, 1 for yes
	public int clearExeRec; 
	// Clinical Notes
	public String clinicalNotes;
	// Medications
	public int patientMeds;
	
	@ManyToMany
	List<Medication> medications;
	
	// Reminders
	public int dietHistoryCollected;
	public int fourDateFoodHistory;
	public int measuringCups;
	
	public int SF12;
	public int DASS21;
	public int Survey;
	
	public int HeartRateRemoved;
	public int Pedometer;
	public int Accelerometer;
	public int Pathology;
	public int UrineBottle;
	
	
	
	
	
	
	

}
