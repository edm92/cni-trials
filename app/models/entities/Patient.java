package models.entities;


import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.twirl.api.Html;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.http.entity.StringEntity;

import be.fnord.ServiceConnector.*;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import com.google.gson.Gson;

import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import util.Helper;

@Entity
public class Patient extends Model{
	@Id
	public Long idPatient;
	
	public String htsNumber;
	public String initials;
	public Date dob;
	public Integer gender; // 0 for ?? and 1 for xy and 2 for xx
							// xy = male , xx = female
	@OneToMany(mappedBy = "patient")
	public List<Assessment> assessments;
	@OneToMany(mappedBy = "patient")
	public List<Council> couciling;
	
	//DB accessor
	public static Finder<Long, Patient> find = new Finder<Long, Patient>(Long.class, Patient.class);

	// Constructor
	public Patient(String HTSNumber, String Initials, Date DOB, Integer gender){
		this.htsNumber = HTSNumber;
		this.initials = Initials;
		this.dob = DOB;
		this.gender = gender;
	}

		public static Patient findById(Long idPatients) {
		return find.where().eq("idPatient", idPatients).findUnique();
	}
	
	public static Patient findByName(String Initials) {
		return find.where().eq("name", Initials).findUnique();
	}
	

	
	public static Page<Patient> find(int page) {
		return find.where()
		                .orderBy("idPatient asc")
		                .findPagingList(10)
		                .setFetchAhead(false)
		                .getPage(page);
	}
	
	
	public static List<Patient> getAllPatients(){
		
		return find.where()
				.findList();
	}
	
	public String toString(){
		try{
		Gson gson= new Gson();
		Variables _idPatient = new Variables();
		_idPatient.name="idPatient";
		_idPatient.value=idPatient + "";

		Variables _htsNumber = new Variables();
		_htsNumber.name="htsNumber";
		_htsNumber.value=htsNumber + "";

		Variables _initials = new Variables();
		_initials.name="initials";
		_initials.value=initials + "";

		Variables _dob = new Variables();
		_dob.name="dob";
		_dob.value=dob + "";

		Variables _gender = new Variables();
		_gender.name="gender";
		_gender.value=gender + "";

		LinkedList<Variables> myList = new LinkedList<Variables>();
		myList.add(_idPatient);
		myList.add(_htsNumber);
		myList.add(_initials);
		myList.add(_dob);
		myList.add(_gender);

		VariableList myVars = new VariableList();
		myVars.variables = myList;
        String postingString = (gson.toJson(myVars));
        
        
		
		return String.format("%s",postingString.toString());
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public Patient bind(String key, String value) {
		Helper.print("\n\n\ndo we reach here?");
		return findById(Long.parseLong(value));
	}

	public F.Option<Patient> bind(String key, Map<String, String[]> data) {
		return F.Option.Some(findById(Long.parseLong(data.get("id_product")[0])));
	}

	public String unbind(String s) {
		return Long.toString(this.idPatient);
	}
	
	public String javascriptUnbind() {
		return Long.toString(this.idPatient);
	}
		
}
