package models.entities;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.twirl.api.Html;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Council extends Model {
	
	@Id 
	public Long idCouncil;
	
	public Date councilDate;
	public int councilType;	// From the models.Type enum	
	
	@ManyToOne
	public Patient patient;
	
	public String clinicalNotes;
	public int confirmedAppoint;
	public int pathologyReferral;
	public int feedbackLetter;
	public int certificatProvided; 
	
	public Double weight;
	public Double bodyFat;

}
