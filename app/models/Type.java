package models;

public enum Type {
	baseLine(0), 
	threeMonth(1), 
	sixMonth(2), 
	nineMonth(3), 
	twelveMonth(4),
	didNotAttend(-5),	// We're gonna use negatives in the DB to store DNA and NA
	didNotAnswer(-10)
	;
	
	public int value;
	Type(int i){
		value = i;
	}
	
	public Type getType(int i){
		switch(i){
		case 0:
			return baseLine;
		case 1: 
			return threeMonth;
		case 2: 
			return sixMonth;
		case 3:
			return nineMonth;
		case 4: 
			return twelveMonth;
		case -5:
			return didNotAttend;
		case -10:
			return didNotAnswer;
		default:
			return baseLine;
		}
	}
}
