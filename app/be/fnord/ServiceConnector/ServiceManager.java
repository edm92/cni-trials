package be.fnord.ServiceConnector;


import java.util.Calendar;



/**
 *	 
 * @author evan -- evan@fnord.be
 *	2014-09-26 - Initial
 */
public class ServiceManager {
	
	
	public void sendUpdate(String idNum, String URL, String message, String type) {
		
		Thread current = (new Thread(new LocationServiceConnector(idNum, URL, message, type)));
		current.start();
		
		
		
	}

}
