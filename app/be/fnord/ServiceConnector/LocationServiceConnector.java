package be.fnord.ServiceConnector;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.LinkedList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import util.Helper;
import be.fnord.ServiceConnector.*;

/**
 * This service connector will output GPS coords to an API endpoint. In the event that the server is not
 * reachable then nothing happens. 
 * Server config details in the config.properties files.
 * 
 * Test cases for this at in ServiceConnectorTest.java (non junit because .. threads) 
 * 
 * @author evan -- evan@fnord.be
 *	2014-09-26 - Initial
 */

public class LocationServiceConnector implements Runnable {
	public static final boolean DEBUG = true; //ServiceConnectorTest.DEBUG;
	
	public static String myURL = Helper.servicePROTO + "://" + Helper.serviceUser + ":" +
			 Helper.servicePass + "@" +
			 Helper.serviceURL + ":" + 
			 Helper.servicePORT + "" +
			 Helper.serviceWAR;

	public static Gson gson;

	Variables idNum = new Variables();
	Variables URL = new Variables();
	Variables type = new Variables();
	Variables msg = new Variables();

	/**
	 * Test of the class
	 * @param args
	 */
	
	
	public LocationServiceConnector(String idNum, String URL, String msg, String type) {
		if(DEBUG) Helper.println("Init");
		
		this.idNum.name = "idNum";
		this.idNum.value = idNum;
		this.URL.name = "URL";
		this.URL.value = URL;
		this.msg.name = "message";
		this.msg.value = msg;
		this.type.name = "type";
		this.type.value = type;

		
	}

	public static String Get(Object postObj , String call, String params){
		String responseString = "";
        try{
        	
        String apiEnd = myURL + call + "?" + params;
        if(DEBUG) Helper.println("Trying " + apiEnd);
        HttpGet post = new HttpGet(apiEnd);
        
        StringEntity  postingString = new StringEntity(gson.toJson(postObj));
        post.setHeader("Content-type", "application/json");
        HttpClient httpClient = getDodgyClient();
        if(httpClient == null) return "Error bypassing ssl";
        
        HttpResponse  response = httpClient.execute(post);
        HttpEntity entity = response.getEntity();
        responseString = EntityUtils.toString(entity, "UTF-8");
        
        if(DEBUG) Helper.println("response: " + response.toString() );
        if(DEBUG) Helper.println(responseString);
    	}catch(Exception eee){
    		if(DEBUG) eee.printStackTrace();
    	}
    	
    	return responseString;

	}
	
	public static String Post(Object postObj , String call){
		String responseString = "";
		
        try{
        	
		Gson gson= new Gson();
        String apiEnd = myURL + call;
        if(DEBUG) Helper.println("Trying " + apiEnd);
        HttpPost post = new HttpPost(apiEnd);
        StringEntity  postingString = new StringEntity(gson.toJson(postObj));
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        HttpClient httpClient = null;
        if(Helper.servicePROTO.toLowerCase().compareTo("https") == 0){
        	httpClient = getDodgyClient();
        	if(httpClient == null) return "Error bypassing ssl";
        }else{
        	httpClient = new DefaultHttpClient(); 
        }
        HttpResponse  response = httpClient.execute(post);
        HttpEntity entity = response.getEntity();
        if(entity != null)
        responseString = EntityUtils.toString(entity, "UTF-8");
        
        
        if(DEBUG) Helper.println("response: " + response.toString() );
        if(DEBUG) Helper.println(responseString);
    	}catch(Exception eee){
    		if(DEBUG) eee.printStackTrace();
    	}
    	
    	return responseString;

	}
	

	public void run() {
		gson= new Gson();
		if(DEBUG) Helper.println("Running");
    	UserLogin usr = new UserLogin(); usr.userId = Helper.serviceUser; usr.password = Helper.servicePass;
    	// Login first
    	String result = Post(usr, "service/login");
    		
		// Start a new job

    	ProcessInstance pi = new ProcessInstance();
    	pi.processDefinitionKey = Helper.serviceUpdateEnd;
    	pi.businessKey = Helper.serviceUpdateEnd;
    	
    	pi.variables.add(idNum);
    	pi.variables.add(type);
    	pi.variables.add(msg);
    	Helper.println("Posting to " + myURL + "service/runtime/process-instances");
    	String id = Post(pi, "service/runtime/process-instances");
		
	}

	
	

	public static HttpClient getDodgyClient(){
		try{
		SSLContext sslContext = SSLContext.getInstance("SSL");
		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

		// set up a TrustManager that trusts everything
		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
		            public X509Certificate[] getAcceptedIssuers() {
//		                    System.out.println("getAcceptedIssuers =============");
		                    return null;
		            }

		            public void checkClientTrusted(X509Certificate[] certs,
		                            String authType) {
//		                    System.out.println("checkClientTrusted =============");
		            }

		            public void checkServerTrusted(X509Certificate[] certs,
		                            String authType) {
//		                    System.out.println("checkServerTrusted =============");
		            }
		} }, new SecureRandom());

		SSLSocketFactory sf = new SSLSocketFactory(sslContext);
		sf.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);

		Scheme httpsScheme = new Scheme("https", 443, sf);
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(httpsScheme);

		
		// apache HttpClient version >4.2 should use BasicClientConnectionManager
		ClientConnectionManager cm = new SingleClientConnManager(schemeRegistry);
		HttpClient httpClient = new DefaultHttpClient(cm);
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

		return httpClient;
		}
		catch(Exception eee){
			return null; 
		}
	}
}
