package be.fnord.ServiceConnector;

import java.util.LinkedList;

public class VariableList {
	public LinkedList<Variables> variables = new LinkedList<Variables>();
	
	public String toString(){
		if(variables == null || variables.size() < 1) return "empty";
		String result = "";
		for(Variables var: variables){
			result += var.toString() + "\n";
		}
		result = result.substring(0, result.length() -1); // remove last newline
		return result;
	}
}
