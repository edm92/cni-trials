package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Logger;

import be.fnord.ServiceConnector.*;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;




import org.mindrot.jbcrypt.BCrypt;


public class Helper {
	public static final boolean _DEBUG = true;
	public static boolean isDebug() { return _DEBUG; };
	public static int YES = 1;
	public static int NO = 0;
    public static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static ServiceManager serviceConnectionManager = new ServiceManager();
    
	public static String serviceUser = "kermit";
	public static String servicePass = "wolfram";
	public static String servicePROTO = "http";
	public static String serviceURL = "localhost";
	public static String servicePORT = "8080";
	public static String serviceWAR = "/rester/";
	public static String serviceUpdateEnd = "newOrderProcess";
	
	public static String playPROTO = "http";
	public static String playURL = "localhost";
	public static String playPORT = "9000";
	public static String playAPI = "/api/";
    
	public static String capitalize(String line)
    {
      return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
		
    /**
     * Init Function, not assumed to be executed
     */
    static boolean __initDone = false;
    static boolean __measureInitDone = false;

    public Helper() {
        if (!__initDone) {
            __initDone = true;
            
        }
    }

    
    
	// Global debugging display levels, in files these are generally propagated
    public static boolean __DEBUG = isDebug();
    public static boolean __INFO = true;
    public static boolean __FATAL = true;
    public static boolean __HIGHDETAILS = false;

    // Do logging?
    public static boolean __LOGGER = false;

    // Debug Levels
    public static final int FATAL = 5;
    public static final int DEBUG = 3;
    public static final int INFO = 1;
    public static final int OFF = 0;

    // Default print level
    static final int __DEFAULTDISPLAY = INFO;
    // Special Characters
    public static String endl = System.getProperty("line.separator");
    public static String tab = "\t";

    public static int indent = 0;

    /**
     * Increase indent level when printing to screen.
     */
    public static void incIndent() {
        ++indent;
    }

    /**
     * Decrease indent level when printing to screen
     */
    public static void decIndent() {
        --indent;
        if (indent < 0) indent = 0;
    }

    public static String dent() {
        String result = "";
        for (int i = 0; i < indent; i++) {
            result += tab;
        }
        ;
        return result;
    }
    
    public static String err(String msg){
    	return println(msg, DEBUG);
    }

    /**
     * Simple toString function, input a message and all indenting is done before returning
     *
     * @param msg
     * @return
     */
    public static String write(String msg) {
    	return write(msg, true);
    }
    
    public static String write(String msg, boolean includeClass){
    	Exception ee = new Exception();
    	int size = ee.getStackTrace().length ;
    	String className = "";
    	for(int i = 0; i < size; i++){
    		if( ee.getStackTrace()[i].getClassName().compareTo(Helper.class.getCanonicalName()) != 0){
    			className = ee.getStackTrace()[i].getClassName();
    			break;
    		}
    	}
    	if(includeClass)
    		return dent() + "[" + className + "] - "+ msg;
    	return dent() + msg;
    }

    /**
     * As write, however a new line is added to the string
     *
     * @param msg
     * @return
     */
    public static String writeln(String msg) {
        return write(msg) + endl;
    }
    
    public static String writeln(String msg, boolean includeClass) {
        return write(msg, includeClass) + endl;
    }

    /**
     * Will both return a string and also display the string to the console at the default display level
     *
     * @param msg
     * @return
     */
    public static String print(String msg) {
        return print(msg, __DEFAULTDISPLAY);
    }

    /**
     * As print, however a new line is added to the string and the print statement.
     * Will also log message to system logger.
     *
     * @param msg
     * @return
     */
    public static String println(String msg) {
        return println(msg, __DEFAULTDISPLAY);
    }

    /**
     * As println, however can set a level of display
     * Will also log message to system logger.
     *
     * @param msg
     * @param displayLevel Either INFO, DEBUG, FATAL
     * @return
     */
    public static String println(String msg, int displayLevel) {
        return print(msg + endl, displayLevel);
    }

    /**
     * As println with display level; however no new line is added.
     * Will also log message to system logger.
     *
     * @param msg
     * @param displayLevel
     * @return
     */
    public static String print(String msg, int displayLevel) {
        String result = write(msg);
        log(result);
        switch (__DEFAULTDISPLAY) {
            case INFO:
            	play.Logger.info(result);
                System.out.print(result);
                break;
            case DEBUG:
            case FATAL:
            	play.Logger.error(result);
                System.err.print(result);
                break;
        }
        ;
        return result;
    }

    /**
     * Write to the system logger.
     *
     * @param msg
     */
    public static void log(String msg) {
        if (__LOGGER) {
            switch (__DEFAULTDISPLAY) {
                case INFO:
                    LOGGER.info(msg);
                    break;
                case DEBUG:
                    LOGGER.warning(msg);
                    break;
                case FATAL:
                    LOGGER.severe(msg);
                    break;
            }
        }
    }


}

