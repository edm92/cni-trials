package controllers;

import java.lang.Thread;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import com.avaje.ebean.Page;
import com.google.common.io.Files;

import models.User;
import models.entities.*;
import play.data.Form;
import play.mvc.Controller;
import static play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import play.twirl.api.Html;
import views.html.lists.*;
import views.html.forms.*;
import views.html.details.*;
import util.*;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
// To include role restrictions add annotion @Restrict(@Group(Application.REPORTS_ROLE))

// Akka Includes
import play.libs.Akka;
import play.libs.F.Promise;
import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.Akka.*;
import java.util.concurrent.Callable;


public class Patients extends Controller{
		
	// Base level form that is used for interactions
	private static final Form<Patient> patientForm = Form.form(Patient.class);

	/*public static Result index() {
		return redirect(routes.Patients.list(0));
	}*/

	protected static Page<Patient> GetPatientList(Page<Patient> patients, Integer page, String filter) {
		try{
		Thread.sleep(5000);
		}catch(Exception e){};
		if(filter.toLowerCase().compareTo("all") == 0)
			patients = Patient.find(page);//findAll();
		else
			patients = Patient.find(page);//findAll();
		return patients;
	}

	public static Promise<Result> list(Integer page, String filter, Integer respond){
		
		// We make an empty promise incase there are erros in the form. 
		Promise<Integer> promiseOfNothing = Promise.promise(
				  new Function0<Integer>() {
					    public Integer apply() {
					    	return 0;
					    }
					  }
					);

		// Check the form binding here
		// Form<Patient> boundForm = patientForm.bindFromRequest();
		// if(boundForm.hasErrors()) {
		// 	util.Helper.println(boundForm.errors().toString());
		// 	flash("error", "Please correct the form below.");
		// 	return badRequest(patientform.render(boundForm));
		// }		
		if(false){	// turn this need to catch form binding error
			return promiseOfNothing.map(
					new Function<Integer, Result>() {
						public Result apply(Integer reports) {
							return badRequest("Emtpy Response");
						}
					}
				);
		}


		// We now make out real promise, to serve content once 
		// we've generated our response. 
		final Page<Patient> dr = null;
		Promise<Page<Patient>> promiseOfReport = Promise.promise(
				  new Function0<Page<Patient>>() {
				    public Page<Patient> apply() {
				    	return GetPatientList(dr, page, filter);
				    }
				  }
				);

		
		// This is run when the promised report is ready
		return promiseOfReport.map(
			new Function<Page<Patient>, Result>() {
				public Result apply(Page<Patient> PatientList) {
					if(PatientList == null) return badRequest("Error finding patient list");

					
					
					if(respond == 1){
						flash("success", String.format("Successfully found patientList"));
						return redirect(routes.Patients.list(page, filter, 0));
					}
					// Default response = 0
					return ok(patientList.render(PatientList, "all", patientForm));

				}
			}
		);
	}


	// public static Result list(Integer page, String filter) {
	// 	Page<Patient> patients = null;
	// 	if(filter.toLowerCase().compareTo("all") == 0)
	// 		patients = Patient.find(page);//findAll();
	// 	else
	// 		patients = Patient.find(page);//findAll();
	// 	return ok(patientList.render(patients, "all", patientForm));
	// }
	

	public static Result newPatient() {
		return ok(patientform.render(patientForm));
	}
	
	public static Result details(Long id) {
		return ok(patientDetails.render(Patient.findById(id)));
	}
	
	public static Result detailsToForm(Long id) {
		Form<Patient> filledForm = patientForm.fill(Patient.findById(id));
		return ok(patientform.render(filledForm));
	}
	


	public static Result save() {
		Form<Patient> boundForm = patientForm.bindFromRequest();
		if(boundForm.hasErrors()) {
			util.Helper.println(boundForm.errors().toString());
			flash("error", "Please correct the form below.");
			return badRequest(patientform.render(boundForm));
		}
		
		Patient patient = boundForm.get();
		Helper.println("Got patient: " + patient.toString());

		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final User user = Application.getLocalUser(session());

		
		if(patient.idPatient == null){
			try{
			String updateInformation = user.email + " added " + patient.toString();
			Helper.serviceConnectionManager.sendUpdate(patient.idPatient + "", "complete", updateInformation, "new");
			patient.save();
			}catch(Exception e){
				e.printStackTrace();
			}
			flash("success", String.format("Successfully added patient %s", patient));
		}
		else {
			patient.update();
			flash("success", String.format("Successfully updated patient %s", patient));
		}
		return redirect(routes.Patients.list(0, "all", 0));
	}
	
	
	public static Result delete(Long id) {
	    final Patient patient = Patient.findById(id);
	    if(patient == null) {
	        return notFound(String.format("Patient %s does not exists.", id));
	    }
	    patient.delete();
	    return redirect(routes.Patients.list(0, "all" , 0));
	}
	


	
}
